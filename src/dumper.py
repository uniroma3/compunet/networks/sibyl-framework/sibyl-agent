import argparse
import signal

from common import dumper_utils

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('filter', type=str)
    parser.add_argument('interfaces', metavar='IFACES', type=str, nargs='+')

    args = parser.parse_args()

    processes = dumper_utils.start_dumpers(args.filter, args.interfaces)


    def handler(signum, frame):
        for p in processes:
            p.terminate()


    signal.signal(signal.SIGTERM, handler)

    for process in processes:
        process.wait()
