import shlex
from subprocess import Popen, DEVNULL


class Client(object):
    @staticmethod
    def exec_async(command, stdout=DEVNULL, stderr=DEVNULL, start_new_session=False, shell=False):
        cmd = shlex.split(command) if not shell else command

        return Popen(cmd, stdout=stdout, stderr=stderr, start_new_session=start_new_session, shell=shell)
