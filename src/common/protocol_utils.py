from exceptions.exceptions import RoutingProtocolError

from .client import Client


def start_protocol(protocol):
    process = None

    if protocol in ['bgp', 'isis', 'open_fabric']:
        process = Client.exec_async('/etc/init.d/frr start')
        process.wait()
        process.terminate()
    elif protocol == 'rift':
        process = Client.exec_async('python3 /rift/rift '
                                    '--telnet-port-file /etc/rift/rift.port --ipv4-multicast-loopback-disable '
                                    '/etc/rift/config.yaml', start_new_session=True)
    elif protocol == 'juniper_rift':
        process = Client.exec_async('cli -c "configure; load set /etc/juniper_rift.conf; commit"')
        process.wait()
        process.terminate()

    if process.returncode is not None and process.returncode != 0:
        raise RoutingProtocolError()
