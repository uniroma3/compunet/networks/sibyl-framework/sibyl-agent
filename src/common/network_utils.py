import json
from subprocess import PIPE, DEVNULL

from .client import Client


def parse_output_string(string):
    output = ''.join(map(lambda x: x.decode('utf-8').strip(), string))
    return json.loads(output)


def get_interface_ip(name):
    process = Client.exec_async('ip -j address show dev %s' % name, stdout=PIPE, stderr=DEVNULL)
    output = process.stdout.readlines()
    process.terminate()

    if process.returncode is not None and process.returncode != 0:
        raise Exception("Interface `%s` not found." % name)

    interfaces_info = parse_output_string(output)
    interface_info = [z for z in interfaces_info if "ifname" in z][0]

    ipv4_address = [z for z in interface_info["addr_info"] if z['family'] == "inet"]
    if not ipv4_address:
        raise Exception("IPv4 address not found.")

    ipv4_address = ipv4_address[0]
    return ipv4_address['local']


def get_interfaces():
    interfaces_info = get_interfaces_info()

    return list(map(lambda x: x['ifname'], interfaces_info))


def get_hostname():
    with open('/etc/hostname', 'r') as hostname_file:
        return hostname_file.readline().strip()


def get_interfaces_info():
    process = Client.exec_async('ip -j link', stdout=PIPE, stderr=DEVNULL)
    output = process.stdout.readlines()
    process.terminate()

    return parse_output_string(output)


def get_interfaces_mac_addresses():
    interfaces_info = get_interfaces_info()

    return dict(map(lambda x: (x['ifname'], x['address']), interfaces_info))


def get_routing_table():
    process = Client.exec_async('ip -j route', stdout=PIPE, stderr=DEVNULL)
    output = process.stdout.readlines()
    process.terminate()
    return parse_output_string(output)
