import logging
import time
from multiprocessing import cpu_count
from multiprocessing.dummy import Pool
from urllib.parse import urlencode

import requests


def _is_downloadable(response):
    header = response.headers
    content_type = header.get('content-type')

    if 'text' in content_type.lower():
        return False
    if 'html' in content_type.lower():
        return False
    if 'json' in content_type.lower():
        return False

    return True


def worker_request(method, workers, endpoint='', args=None):
    worker_responses = {}
    max_retries = 10
    backoff_factor = 0.25

    def callback(item):
        (name, ip) = item
        node_args = args[name] if args is not None else None

        logging.info('[CONTROLLER] Sending request to node `%s` (IP=%s) to endpoint `%s` with args `%s`...' %
                     (name, ip, endpoint, node_args))

        base_url = 'http://%s:3000/%s' % (ip, endpoint)

        retries = 0
        while retries < max_retries:
            try:
                if method.lower() == 'get':
                    query_params = "" if args is None else "?" + urlencode(node_args)
                    response = requests.get('%s%s' % (base_url, query_params), timeout=None)
                elif method.lower() == 'post':
                    response = requests.post('%s' % base_url, json=node_args, timeout=None)
                else:
                    raise Exception('Method `%s` not supported' % method)

                response.raise_for_status()

                if not _is_downloadable(response):
                    worker_responses[name] = response.json()
                else:
                    worker_responses[name] = response.content

                break
            except Exception as e:
                logging.info(f'[CONTROLLER] Node `{name}` threw exception `{str(e)}`.')
                if 'No route to host' in str(e):
                    logging.info(f'[CONTROLLER] Unable to contact node `{name}`, skipping retries.')
                    break

                logging.info(f'[CONTROLLER] {max_retries - retries:d} retries for node `{name}` remaining.')
                retries += 1
                time.sleep(backoff_factor * (2 ** (retries - 1)))

    pool = Pool(cpu_count())
    pool.map(func=callback, iterable=workers)

    return worker_responses
