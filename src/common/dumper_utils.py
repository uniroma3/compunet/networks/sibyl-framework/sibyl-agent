import os
import signal
import tarfile

from .client import Client

DUMP_PATH = '/dumps'


def start_dumpers(dumper_filter, interfaces):
    if not os.path.isdir(DUMP_PATH):
        os.mkdir(DUMP_PATH)

    processes = []

    for iface in interfaces:
        process = start_dumper(dumper_filter, iface)

        while process.returncode is not None and process.returncode != 0:
            process = start_dumper(dumper_filter, iface)

        processes.append(process)

    return processes


def start_dumper(dumper_filter, iface):
    if dumper_filter == 'bgp':
        packet_filter = "tcp port 179 and (((ip[2:2] - ((ip[0]&0xf)<<2)) - ((tcp[12]&0xf0)>>2)) != 0)"
    elif dumper_filter == 'isis' or dumper_filter == 'open_fabric':
        packet_filter = "iso"
    elif 'rift' in dumper_filter:
        packet_filter = "udp && udp[8] == 0xa1 && udp[9] == 0xf7"
    else:
        packet_filter = dumper_filter

    dump_file_name = os.path.join(DUMP_PATH, "%s.pcap" % iface)
    command = f'tcpdump -Q inout -U -enni {iface} -w {dump_file_name} "{packet_filter}"'
    process = Client.exec_async(command, shell=True)

    return process


def start_dumper_process(dumper_filter, interfaces):
    process = Client.exec_async('/test_agent/dumper %s %s' % (dumper_filter, " ".join(interfaces)))

    return process.pid


def stop_dumper_process(pid):
    os.kill(pid, signal.SIGTERM)


def pack_dump_files():
    dump_tar = os.path.join('/tmp', 'dumps.tar.gz')

    with tarfile.open(dump_tar, "w:gz") as tar_file:
        for dump_file in os.listdir(DUMP_PATH):
            if '.pcap' not in dump_file:
                continue

            tar_file.add(os.path.join(DUMP_PATH, dump_file), arcname="/%s" % dump_file)

    return dump_tar
