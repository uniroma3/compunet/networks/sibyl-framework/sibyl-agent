import io
import logging
import os
import shutil
import tarfile
import time
from pathlib import Path

import requests
from flask import Flask
from flask import abort
from flask import jsonify
from flask import request
from flask import send_file

from common import network_utils, protocol_utils, worker_utils, dumper_utils

CONTROLLER_URL = 'http://controller-agent:3000' if not os.environ.get("CONTROLLER_URL") else \
    os.environ.get("CONTROLLER_URL")
CONTROLLER_AGENT = (os.environ.get("CONTROLLER_AGENT") == "true")

if CONTROLLER_AGENT:
    os.mkdir('/tmp/nodes')

    logging.basicConfig(level=logging.DEBUG)
    logging.info('[CONTROLLER] Controller agent, waiting for nodes to join...')

app = Flask(__name__)


def _join_controller():
    url = '%s/join' % CONTROLLER_URL
    try:
        # Try to get interface net0 (only available in Kubernetes pods)
        network_utils.get_interface_ip('net0')
        # If no exception is raised, is Kubernetes, get eth0 ip
        ip = network_utils.get_interface_ip('eth0')
    except Exception:
        # net0 is not found, this is Docker, get bridged interface IP (the last with ethX name)
        interfaces = [x for x in network_utils.get_interfaces() if 'eth' in x]
        # Get last interface with ethX name
        interfaces = sorted(interfaces, key=lambda x: int(x.replace('eth', '')))
        ip = network_utils.get_interface_ip(interfaces[-1])

    name = network_utils.get_hostname()

    logging.info('[WORKER] Joining controller to `%s`, with IP=%s and NAME=%s' % (url, ip, name))

    try:
        response = requests.get(url, {
            'ip': ip,
            'name': name
        })

        if response.status_code != 200:
            time.sleep(5)
            _join_controller()
    except Exception as e:
        time.sleep(5)
        _join_controller()


@app.route('/')
def home():
    role = 'the Controller' if CONTROLLER_AGENT else 'a Worker'
    return "Welcome to the Fat Tree Test Agent! :)<br />I am %s Node." % role


@app.route('/join')
def join():
    if CONTROLLER_AGENT:
        ip = request.args.get('ip')
        name = request.args.get('name')

        nodes_files = os.listdir('/tmp/nodes')
        node_info_file = list(filter(lambda x: ('%s~' % name) in x, nodes_files))
        if len(node_info_file) > 0:
            os.remove(os.path.join('/tmp/nodes', node_info_file[0]))

        Path('/tmp/nodes/%s~%s' % (name, ip)).touch()

        logging.info('[CONTROLLER] Node `%s` with IP `%s` joined.' % (name, ip))

        return jsonify({'status': 'OK'})
    else:
        abort(500)


@app.route('/nodes')
def get_joined_nodes():
    nodes_files = os.listdir('/tmp/nodes')
    nodes = {}

    for node_file in nodes_files:
        (name, ip) = node_file.split('~')
        nodes[name] = ip

    return jsonify(nodes)


@app.route('/flush')
def flush():
    shutil.rmtree('/tmp/nodes', ignore_errors=True)
    os.mkdir('/tmp/nodes')

    return jsonify({'status': 'OK'})


@app.route('/system-time', methods=['POST'])
def get_system_time():
    if not CONTROLLER_AGENT:
        logging.info('[WORKER] Getting System Time...')
        return jsonify({'time': time.time()})
    else:
        nodes = request.json['nodes']

        system_times = worker_utils.worker_request('POST', nodes.items(), 'system-time')
        return jsonify(system_times)


@app.route('/network-interfaces', methods=['POST'])
def get_network_interfaces():
    if not CONTROLLER_AGENT:
        logging.info('[WORKER] Dumping network interfaces...')
        return jsonify(network_utils.get_interfaces_info())
    else:
        nodes = request.json['nodes']

        network_interfaces = worker_utils.worker_request('POST', nodes.items(), 'network-interfaces')
        return jsonify(network_interfaces)


@app.route('/interfaces-mac-addresses', methods=['POST'])
def get_interfaces_mac_addresses():
    if not CONTROLLER_AGENT:
        logging.info('[WORKER] Dumping interfaces MAC Addresses...')
        return jsonify(network_utils.get_interfaces_mac_addresses())
    else:
        nodes = request.json['nodes']

        interfaces_mac_addresses = worker_utils.worker_request('POST', nodes.items(), 'interfaces-mac-addresses')
        return jsonify(interfaces_mac_addresses)


@app.route('/routing-table', methods=['POST'])
def get_routing_table():
    if not CONTROLLER_AGENT:
        logging.info('[WORKER] Dumping routing table...')
        return jsonify(network_utils.get_routing_table())
    else:
        nodes = request.json['nodes']

        routing_tables = worker_utils.worker_request('POST', nodes.items(), 'routing-table')
        return jsonify(routing_tables)


@app.route('/start-protocol', methods=['POST'])
def start_protocol():
    protocol = request.json['protocol']

    if not CONTROLLER_AGENT:
        logging.info('[WORKER] Starting protocol `%s`...' % protocol)

        try:
            protocol_utils.start_protocol(protocol)
        except Exception:
            abort(500)

        return jsonify({'status': 'OK'})
    else:
        nodes = request.json['nodes']

        args = {name: {'protocol': protocol} for name in nodes.keys()}
        worker_responses = worker_utils.worker_request('POST', nodes.items(), 'start-protocol', args)
        return jsonify({'status': 'OK'}) if all(map(lambda x: x['status'] == 'OK', worker_responses.values())) \
            else abort(500)


@app.route('/start-dumpers', methods=['POST'])
def start_dumpers():
    interfaces = request.json['interfaces']
    protocol = request.json['protocol']

    if not CONTROLLER_AGENT:
        logging.info('[WORKER] Starting dumper process on interfaces %s for protocol %s...' %
                     (str(interfaces), protocol)
                     )
        dumper_pid = dumper_utils.start_dumper_process(protocol, interfaces)
        logging.info('[WORKER] Dumper started with PID=%d...' % int(dumper_pid))

        return jsonify({'pid': dumper_pid})
    else:
        nodes = request.json['nodes']

        args = {key: {"interfaces": value, "protocol": protocol} for key, value in interfaces.items()}
        worker_dumper_pids = worker_utils.worker_request('POST', nodes.items(), 'start-dumpers', args)

        return jsonify({name: int(response['pid']) for name, response in worker_dumper_pids.items()})


@app.route('/stop-dumpers', methods=['POST'])
def stop_dumpers():
    if not CONTROLLER_AGENT:
        pid = int(request.json['pid'])
        logging.info('[WORKER] Stopping dumper process with PID=%d...' % pid)
        dumper_utils.stop_dumper_process(pid)

        return jsonify({'status': 'OK'})
    else:
        nodes = request.json['nodes']
        dumpers_pids = request.json['dumper_pids']

        args = {key: {"pid": value} for key, value in dumpers_pids.items()}
        worker_responses = worker_utils.worker_request('POST', nodes.items(), 'stop-dumpers', args)
        return jsonify({'status': 'OK'}) if all(map(lambda x: x['status'] == 'OK', worker_responses.values())) \
            else abort(500)


@app.route('/get-dumps', methods=['POST'])
def get_dumps():
    if not CONTROLLER_AGENT:
        logging.info('[WORKER] Packing .pcap dumps in a .tar.gz archive...')

        dump_tar = dumper_utils.pack_dump_files()

        logging.info('[WORKER] Sending file %s to controller...' % dump_tar)

        try:
            return send_file(dump_tar, download_name='dumps.tar.gz')
        except FileNotFoundError:
            abort(404)
    else:
        nodes = request.json['nodes']

        worker_dumper_tars = worker_utils.worker_request('POST', nodes.items(), 'get-dumps')

        dump_file_path = os.path.join('/tmp', 'dumps.tar.gz')
        with tarfile.open(dump_file_path, 'w:gz') as tar_file:
            for name, worker_dumper_tar_file in worker_dumper_tars.items():
                tarinfo = tarfile.TarInfo(os.path.join("/", "%s.tar.gz" % name))
                tarinfo.size = len(worker_dumper_tar_file)

                tar_file.addfile(tarinfo, io.BytesIO(worker_dumper_tar_file))

        try:
            return send_file(dump_file_path, download_name='dumps.tar.gz')
        except FileNotFoundError:
            abort(404)


if __name__ == '__main__':
    if not CONTROLLER_AGENT:
        logging.basicConfig(filename="/var/log/agent.log", level=logging.DEBUG)
        _join_controller()

    app.run(host='0.0.0.0', port=3000)
