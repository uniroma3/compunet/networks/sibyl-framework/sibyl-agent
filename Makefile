#!/usr/bin/make -f

.PHONY: all docker-build-controller-agent docker-build-binaries-agent

all: docker-build-controller-agent docker-build-binaries-agent

docker-build-controller-agent:
	cp -R src/ controller-agent-image/
	cd controller-agent-image; docker build -t skazza/agent-controller .
	rm -rf controller-agent-image/src

docker-build-binaries-agent:
	cp -R src/ agent-binaries-image/
	cd agent-binaries-image; docker build -t skazza/agent-binaries .
	rm -rf agent-binaries-image/src