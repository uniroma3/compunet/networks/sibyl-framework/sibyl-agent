# -*- mode: python ; coding: utf-8 -*-

block_cipher = None


a = Analysis(['src/agent.py'],
             pathex=['src/'],
             binaries=[],
             datas=[],
             hiddenimports=['common',
                            'common.client',
                            'common.network_utils',
                            'common.protocol_utils',
                            'common.worker_utils',
                            'exceptions',
                            'exceptions.exceptions',
                            'process',
                            'process.rift_process',
                            'process.screen_process',
                            'pkg_resources.py2_warn'
                            ],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data, cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          [],
          name='agent',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          upx_exclude=[],
          runtime_tmpdir=None,
          console=True)