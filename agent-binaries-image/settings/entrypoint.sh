#!/bin/bash

if [ "$IS_K8S" = "true" ]; then
  rm -f /shared/test_agent.tar.gz
  cp -f /var/www/html/binaries.tar.gz /shared/test_agent.tar.gz
else
  service nginx start
fi

sleep infinity