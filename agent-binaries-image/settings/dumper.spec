# -*- mode: python ; coding: utf-8 -*-

block_cipher = None


a = Analysis(['src/dumper.py'],
             pathex=['src/'],
             binaries=[],
             datas=[],
             hiddenimports=['common',
                            'common.client',
                            'common.dumper_utils',
                            'pkg_resources.py2_warn'
                            ],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data, cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          [],
          name='dumper',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          upx_exclude=[],
          runtime_tmpdir=None,
          console=True)