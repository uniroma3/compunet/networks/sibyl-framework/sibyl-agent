# Sibyl Agent

A REST Service composed of a controller and several worker agents. Once deployed it allows the controller agent to perform several action (e.g., causing an interface failure, starting services) on nodes with a worker agent.

It is used in the [Sibyl](https://gitlab.com/uniroma3/compunet/networks/sibyl-framework/sibyl) framework to test protocol implementations for fat trees. 

## APIs
The Sibyl Agent exposes the following API: 

| API                         | Description                                                           | 
|-----------------------------|-----------------------------------------------------------------------|
| `/join`                     | Used by the controller agent to verify which nodes joined the service |
| `/nodes`                    | Return a json file containing the list of the joined nodes            | 
| `/flush`                    | Flush the list of joined nodes                                        | 
| `/system-time`              | Get the system times from worker nodes                                | 
| `/network-interfaces`       | Get interfaces info from the worker nodes                             | 
| `/interfaces-mac-addresses` | Get interfaces MAC Addresses from the worker nodes                    |
| `/routing-table`            | Get the routing table from the worker nodes                           | 
| `/start-protocol`           | Start a routing protocol on worker nodes                              |
| `/start-dumpers`            | Start a dumper on the interface of the worker nodes                   |
| `/stop-dumpers`             | Start to dump the interface of the worker nodes                       |
| `/get-dumps`                | Get  the dumps on the interface of the worker nodes                   |

